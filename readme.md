The program will fail to compile on windows

Go into Cargo.toml and turn split-debuginfo = "unpacked" into #split-debuginfo = "unpacked"
This is commenting out the line, this disables it.

On a Ryzen 3200G this bloated program took 7-13 mins to compile

Building can be done on these commands (if you have rust/rustup installed and you are on Linux/Mac)

git clone https://codeberg.org/Rameez10/ramterm
cd ramterm
cargo build --release
target/release
cargo run --release --bin wezterm
sudo cp wezterm wezterm-gui wezterm-mux-server /usr/bin
sudo chmod u+x /usr/bin/wezterm wezterm-gui wezterm-mux-server


Bloat

This terminal is bloated 

St - 0.124mb
Wezterm + wezterm-gui + wezterm-mux-server - 117mb
Ramterm (Better build of Wezterm + wezterm-gui + wezterm-mux-server) - 80mb

You may want to use https://github.com/lukesmithxyz/st instead